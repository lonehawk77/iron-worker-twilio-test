require 'iron_worker_ng'
require 'uber_config'

@config = UberConfig.load
@iw = IronWorkerNG::Client.new

tasks = @iw.schedules_list
tasks.each do |task|
  @iw.schedules.cancel task.id
end

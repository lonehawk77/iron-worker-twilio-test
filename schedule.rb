require 'iron_worker_ng'
require 'uber_config'
require 'time'
require 'active_support/core_ext/date'
require 'active_support/core_ext/numeric'

@config = UberConfig.load
@iw = IronWorkerNG::Client.new

puts @config
schedules = 7
at = 7
args = ARGV.join(' ')

match = /(?<schedules>--schedules=\d+)/.match(args)

if match && match['schedules']
  puts "Schedules: " + match['schedules'].gsub('--schedules=','')
  schedules = match['schedules'].gsub('--schedules=','').to_i
end

match = /(?<at>--at=\d+\.?\d*)/.match(args)

if match && match['at']
  puts match['at']
  at = match['at'].gsub('--at=','').to_f
end

puts at

schedules.times do |i|
  @iw.schedules.create("voice",
                       @config,
                       {
                           :start_at => Time.now.gmtime.tomorrow.at_beginning_of_day + at.hours + i.days,
                       })
end